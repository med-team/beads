#include "spotDocument.h"
#include <QFileInfo>

spotDocument::spotDocument(CalcWriterInterface * p_writer) {
    _p_writer = p_writer;
}

spotDocument::spotDocument() {
}

spotDocument::~spotDocument() {
}

void spotDocument::open() {

    _p_writer->writeCell("Spot");
    _p_writer->writeCell("Volume");
    _p_writer->writeCell("x");
    _p_writer->writeCell("y");
    _p_writer->writeCell("min");
    _p_writer->writeCell("surface");
    _p_writer->writeCell("background");
    _p_writer->writeCell("vol-bckgnd");
    _p_writer->writeLine();
}

void spotDocument::write_spot(const spot & thespot) {
    //std::ostringstream tag;
  
    _p_writer->writeCell(thespot.get_number());
    _p_writer->writeCell((int) thespot.get_vol());
    _p_writer->writeCell(thespot.get_tx());
    _p_writer->writeCell(thespot.get_ty());
    _p_writer->writeCell((int) thespot.get_tmin());
    _p_writer->writeCell((int)  thespot.get_area());
    _p_writer->writeCell((int) thespot.get_bckgnd());
    _p_writer->writeCell((int) (thespot.get_vol() - thespot.get_bckgnd()));
    _p_writer->writeLine();
    //return (tag.str().c_str());
}

void spotDocument::close() {
    qDebug() << "spotDocument::close begin";
    _p_writer->close();
    qDebug() << "spotDocument::close end";
}

void spotDocument::write_detection(detection & the_detection) {

    detection::iterator it;
    for (it = the_detection.begin(); it != the_detection.end(); ++it) {
        spot & the_spot = (*it).second;
        write_spot(the_spot);
    }
}

